'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function () {
    gulp.src('./sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'Firefox 14']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'));
});


gulp.task('watch', function () {
    gulp.watch(['./sass/**/*.scss'], ['sass']);
});